import logging from "external:@totates/logging/v1";
import { baseUrl, componentName, styles } from "./blaze.js";
import component from "./component.json";

const logger = logging.getLogger("@totates/blaze-css");
// logger.setLevel("WARN");

export const styleNames = styles.map(name => `style.${name}`);
export const urls = Object.fromEntries(
    styles.map(name => [`style.${name}`, `${baseUrl}/${name}.css`])
);

export function sort(names) {
    names.sort((a, b) => {
        if (a === b)
            return 0;
        return styleNames.indexOf(a) < styleNames.indexOf(b) ? -1 : +1;
    });
}

function known(names) {
    const unknown = new Set();
    const selected = new Set();
    for (const name of names)
        (styleNames.indexOf(name) === -1 ? unknown : selected).add(name);
    if (unknown.size)
        logger.warn("unknown styles:", unknown);
    return Array.from(selected);
}

export function resources(names) {
    return known(names).map(styleName => `${componentName}:${styleName}`);
}

export function urlsFromManifest(index) {
    const componentId = `${component.name}/v${component.version.split(".")[0]}`;
    if (!(componentId in index.select))
        throw new Error(`no "${componentId}" entry in element manifest`);
    const names = known(index.select[componentId]);
    sort(names);
    return names.map(style => urls[style]);
}

/* visibility */
export const uDisplayNone = "u-display-none";

export function show(el) {
    el.classList.remove(uDisplayNone);
}

export function hide(el) {
    el.classList.add(uDisplayNone);
}

export function isVisible(el) {
    return !el.classList.contains(uDisplayNone);
}

export function toggleVisibility(...elements) {
    elements.forEach(el => el.classList.toggle(uDisplayNone));
}

export function showIf(test, el) {
    el.classList[test ? "remove" : "add"](uDisplayNone);
}

export function filter(test, elements) {
    elements.forEach(el => showIf(test(el), el));
}

export function showIfElse(test, ...pair) { /* [ thenElement, elseElelement ] */
    if (!test) pair.reverse();
    show(pair[0]);
    hide(pair[1]);
}

export function toggleModal(el) {
    const classList = el.querySelector(".o-modal").classList;
    const visible = classList.contains("o-modal--visible");
    const op = visible ? "remove" : "add";
    classList[op]("o-modal--visible");

    const overlay = el.querySelector(".c-overlay");
    if (overlay)
        overlay.classList[op]("c-overlay--visible");

    return !visible;
}

export function setupTabs(el) {
    for (const tabs of el.querySelectorAll(".c-tabs")) {
        const headings = tabs.querySelectorAll(".c-tab-heading");
        function select(e) {
            let i = 0;
            for (const heading of headings) {
                let op;
                if (heading === e.target) {
                    op = "add";
                    let j = 0;
                    for (const tab of tabs.querySelectorAll(".c-tabs__tab")) {
                        if (i === j)
                            tab.removeAttribute("hidden");
                            // tab.firstElementChild.focus();
                        else
                            tab.setAttribute("hidden", "");
                        j++;
                    }
                }
                else
                    op = "remove";
                heading.classList[op]("c-tab-heading--active");
                i++;
            }
        }
        for (const heading of headings)
            heading.onclick = select;
    }
}

export function setupSlotVisibility(slot, empty) {
    slot.addEventListener("slotchange", e => {
        // const slot = e.target;
        const nodes = slot.assignedNodes();
        if (nodes.length) {
            show(slot);
            hide(empty);
        }
        else {
            show(empty);
            hide(slot);
        }
    });
}

export default {
    filter,
    hide,
    isVisible,
    resources,
    setupSlotVisibility,
    setupTabs,
    show,
    showIf,
    showIfElse,
    sort,
    styleNames,
    toggleModal,
    toggleVisibility,
    uDisplayNone,
    urls,
    urlsFromManifest
};
