import assert from "assert";
import fs from "fs-extra";
import path from "path";
import { Component, minifyCss } from "totates";
import { renderSync } from "node-sass";

import componentData from "./component.json" assert { type: "json" };
const component = new Component(componentData);

const { destDir, manifest } = component;
const blazeDir = "node_modules/@blaze/css/src/scss";

async function writeResource(styleName, filename, data) {
    // TODO: render and minify in one step, and use addFileResource
    const result = renderSync({ data, includePaths: [blazeDir] });
    const content = minifyCss(result.css);
    await manifest.addResourceFromContent(
        `style.${styleName}`, content, "text/css", destDir, path.join("css", filename)
    );
}

const blazePlugin = {
    name: "blaze",
    async buildStart() {
        const base64Icon = (
            "R0lGODlhDwAUAIABAAAAAP///yH5BAEAAAEALAAAAAAP" +
            "ABQAAAIXjI+py+0Po5wH2HsXzmw//lHiSJZmUAAAOw=="
        );
        await manifest.addResourceFromContent(
            "icon", Buffer.from(base64Icon, "base64"), "image/png", destDir, "icon.png"
        );

        const dataURL = `data:image/png;base64,${base64Icon}`;
        const filenames = await fs.readdir(blazeDir);
        for (const filename of filenames) {
            const filePath = path.join(blazeDir, filename);
            const stats = await fs.stat(filePath);
            if (stats.isDirectory())
                continue;
            let content = await fs.readFile(filePath, "utf8");
            if (filename.startsWith("_")) {
                if (filename === "_variables.scss") {
                    for (const name of ["host", "root"]) {
                        const styleName = `variables.${name}`;
                        content = content.replace(/^:root,\n:host/u, `:${name}`);
                        await writeResource(styleName, `${styleName}.css`, content);
                    }
                }
                else {
                    if (filename === "_components.cards.scss" ||
                        filename === "_components.inputs.scss")
                        content = content.replace(dataURL, "../icon.png");
                    const [, styleName] = /^_(.*)\.scss$/u.exec(filename);
                    await writeResource(styleName, `${styleName}.css`, content);
                }
            }
            else {
                assert.strictEqual(filename, "blaze.scss");
                const styles = content.trim().split("\n").map(line => line.split("'")[1]);
                assert.strictEqual(styles[0], "variables");
                styles.shift();
                styles.unshift("variables.host", "variables.root");
                await fs.writeFile("blaze.js", (
                    `export const baseUrl = "${path.join(manifest.baseUrl, "css")}";\n` +
                    `export const componentName = "${manifest.name}";\n` +
                    `export const styles = ${JSON.stringify(styles)};`
                ), "utf8");
            }
        }
    }
};

const config = component.getConfig();
config.plugins.push(blazePlugin);
export default config;
